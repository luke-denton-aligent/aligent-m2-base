var config = {
    map: {
        '*': {
            showHideElement:   'js/ShowHideElement',
            carousel:          'js/Carousel',
            restrictHeight:    'js/RestrictHeight',
            toggleTabElements: 'js/ToggleTabElements'
        }
    }
};
