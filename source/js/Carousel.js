/**
 * Carousel
 *
 * Use this class to create either a manual carousel (use has to click on the next/previous buttons) or an automatica
 * carousel (items will automatically change to the next, but users can still click the next/previous buttons to change
 * what is showing).
 *
 * The class name `carousel-item-active` is toggled between the carousel items.
 *
 * Visibility of the items is up to you, and is to be controlled via CSS. This allows you to easily add transition animations as well
 *
 * PARAMETERS
 *
 *   N.B. All parameters are optional
 *
 *   {string}  prevBtn  CSS selector for the button/element to use as the trigger to move to the previous item. Default `.carousel-prev`
 *   {string}  nextBtn  CSS selector for the button/element to use as the trigger to move to the next item. Default `.carousel-next`
 *   {string}  items    CSS selector for the <ul> list of carousel items. Default `.carousel-items`
 *   {Boolean} autoplay Indicate if the carousel should auto change. Default `false`
 *   {int}     delay    The amount of time, in ms, to wait before changing to the next item. Only used if `autoplay` = true. Default `5000`
 *
 * USAGE
 *
 *   <div class="carousel-container" data-mage-init='"carousel":{}'>
 *       <button class="previous-button carousel-prev"></button>
 *       <ul class="items carousel-items">
 *           <li class="carousel-item-active">Item 1</li>
 *           <li>Item 2</li>
 *           <li>Item 3</li>
 *       </ul>
 *       <button class="next-button carousel-next"></button>
 *   </div>
 *
 *   N.B. The above example passed no options, so it's using the default values for all parameters
 *
 *   - OR -
 *
 *   <script type="text/x-magento-init">
 *     {
 *         ".carousel-container": { //The element to bind the class to
 *             "carousel": {
 *                 "prevBtn": ".previous-button",
 *                 "nextBtn": ".next-button",
 *                 "items": ".items",
 *                 "autoplay": true,
 *                 "delay": 10000
 *             }
 *         }
 *     }
 *   </script>
 *
 *   - OR -
 *
 *   define(['carousel'], function(createCarousel) {
 *      return function() {
 *          const config = {
 *              "prevBtn": ".previous-button",
 *              "nextBtn": ".next-button",
 *              "items": ".items",
 *              "autoplay": true,
 *              "delay": 10000
 *          };
 *          const element = $('.carousel-container')
 *          const Carousel = createCarousel(config, element)
 *      }
 *   })
 *
 * EVENTS
 *
 *   There are 6 events that are thrown. They are:
 *
 *   - before_aligent_carousel_toggle
 *       Thrown before the carousel is about to toggle from one item to the next
 *
 *   - before_aligent_carousel_hide_prev
 *       Thrown before the carousel is about to hide the previous item
 *       Providing a second parameter to jQuery's .on() function will give a jQuery object for the item being hidden
 *       e.g. `$carouselEl.on('before_aligent_carousel_hide_prev', function(e, data) { //data.previous = the item being hidden })`
 *
 *   - after_aligent_carousel_hide_prev
 *       Thrown after the carousel has hidden the previous item
 *       Providing a second parameter to jQuery's .on() function will give a jQuery object for the item hidden
 *       e.g. `$carouselEl.on('after_aligent_carousel_hide_prev', function(e, data) { //data.previous = the item hidden })`
 *
 *   - before_aligent_carousel_show_next
 *       Thrown before the carousel is about to show the next item
 *       Providing a second parameter to jQuery's .on() function will give a jQuery object for the item about to be shown
 *       e.g. `$carouselEl.on('before_aligent_carousel_show_next', function(e, data) { //data.next = the item about to be shown })`
 *
 *   - after_aligent_carousel_show_next
 *       Thrown after the carousel has hidden the previous item
 *       Providing a second parameter to jQuery's .on() function will give a jQuery object for the item hidden
 *       e.g. `$carouselEl.on('after_aligent_carousel_hide_prev', function(e, data) { //data.previous = the item hidden })`
 *
 *   - after_aligent_carousel_toggle
 *       Thrown after the carousel has toggled from one item to the next
 */
define(['jquery'], function($) {
    'use strict';

    class Carousel {

        /**
         * Constructor
         *
         * @param {Object}  config  Configuration options passed in through JSON
         * @param {Element} element The element that this class was bound to
         *
         * @returns {null}
         */
        constructor(config, element) {
            const defaultConfig = {};
            // Merge default config options and provided config
            this.config   = {...defaultConfig, ...config};
            this.$element = $(element);

            this.prevBtnSelector = this.config.prevBtn || '.carousel-prev';
            this.nextBtnSelector = this.config.nextBtn || '.carousel-next';
            this.itemsSelector   = this.config.items || '.carousel-items';

            this._attachListeners();

            if (this.config.autoplay) {
                this.interval = this.startAutoplay();
            }
        }

        /**
         * Set up the timing for autoplay of the carousel
         *
         * @returns {int} The ID of the interval created
         */
        startAutoplay() {
            const delay = this.config.delay || 5000;

            return window.setInterval(() => {
                this.carouselToggleNext();
            }, delay);
        }

        /**
         * Remove the old intervale and start a new one
         * This is required if a user changes to the next carousel item themselves, the timer should reset
         *
         * @returns {int}
         */
        restartAutoplay() {
            if (this.interval) {
                window.clearInterval(this.interval)
            }

            this.interval = this.startAutoplay();
        }

        /**
         * Attach event listeners to elements
         *
         * @private
         * @returns {null}
         */
        _attachListeners() {
            this.$element
                .on('click', this.prevBtnSelector, () => {
                    if (this.config.autoplay) {
                        this.restartAutoplay();
                    }

                    this.carouselToggleNext(false);
                })
                .on('click', this.nextBtnSelector, () => {
                    if (this.config.autoplay) {
                        this.restartAutoplay();
                    }

                    this.carouselToggleNext(true);
                });
        }

        /**
         * Toggle the carousel to the next element
         *
         * @param {boolean} showNext [Optional] Provide false if you wish the carousel to go to the previous element
         *
         * @returns {null}
         */
        carouselToggleNext(showNext = true) {
            const $items = this.$element.find(this.itemsSelector);
            const $current = $items.find('li.carousel-item-active'); // Get currently visible item
            let $next; // The next carousel item to show, not necessarily the next item in the DOM

            if (showNext) {
                $next = $current.next("li"); // Get next list item
            } else {
                $next = $current.prev("li"); // Get previous list item
            }

            if ($next.length === 0) {
                $next = (showNext) ?
                    $items.find("li:first-child") : $items.find("li:last-child");
            }

            this._throwEvent('before_aligent_carousel_toggle');

            this._throwEvent('before_aligent_carousel_hide_prev', {
                previous: $current
            });
            $current.toggleClass("carousel-item-active", false);
            this._throwEvent('after_aligent_carousel_hide_prev', {
                previous: $current
            });

            this._throwEvent('before_aligent_carousel_show_next', {
                next: $next
            });
            $next.toggleClass("carousel-item-active", true);
            this._throwEvent('after_aligent_carousel_show_next', {
                next: $next
            });

            this._throwEvent('after_aligent_carousel_toggle');
        }

        /**
         *
         * @param {string} event Name of event to throw
         * @param {Object} extraParams [Optional] Extra parameters to send with the event
         *
         * @private
         * @returns {null}
         */
        _throwEvent(event, extraParams = {}) {
            this.$element.trigger(event, extraParams);
        }
    }

    return function(config, element) {
        var carousel = new Carousel(config, element);
        return carousel;
    };
});
