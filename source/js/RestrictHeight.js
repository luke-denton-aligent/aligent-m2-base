/**
 * Class RestrictHeight
 *
 * Restrict the height of an element, hiding the rest behind a "show more" button of some sort
 *
 * The class will toggle a class name, `.toggle-height-restriction`, which can be targetted in CSS to apply/remove
 * necessary height restrictions and transitions
 *
 * In order to allow for smooth height transitions from a fixed number to an "auto" number, the class will dynamically
 * determine the height of the container when all children are showing, and apply that to the container as the max height.
 * This means that in your CSS, you only have to worry about the starting value, and effectively think of the other value
 * as 'auto'.
 *
 * PARAMETERS
 *
 *   {string}  toggleBtn           CSS selector for the element that will toggle the height restriction
 *   {Boolean} initiallyRestricted [Optional] Boolean indicating the starting state of the element the height
 *                                 restriction will affect [Default: true]
 *   {int}     minWidth            [Optional] The minimum width, inclusive, in px, of the viewport for the class to take effect
 *                                 For example, enforce the class to only work on large devices
 *   {int}     maxWidth            [Optional] The maximum width, inclusive, in px, of the viewport for the class to take effect
 *                                 For example, enforce the class to only work on small devices
 *   {string} programatic          [Optional] Flag to indicate if the instance is going to be controlled by outside JS
 *                                 Default: false
 *
 * USAGE
 *
 *   <dl class="product-specs-list" data-mage-init='"restrictHeight":{"toggleBtn":".btn__show-all"}'>
 *       <dt>Type</dt>
 *       <dd>Quad-Copter</dd>
 *       <dt>Main Rotor Diameter</dt>
 *       <dd>1.97 in (50.0mm)</dd>
 *       <dt>Gross Weight</dt>
 *       <dd>0.58 oz (16.5 g)</dd>
 *       <dt>Length</dt>
 *       <dd>5.5 in (140mm)</dd>
 *   </dl>
 *   <button class="btn__show-all">Show All</button>
 *
 * - OR -
 *
 *   <script type="text/x-magento-init">
 *     {
 *       "restrictHeight": {
 *         ".product-specs-list": {
 *           "toggleBtn": ".btn__show-all",
 *           "initiallyRestricted": "true",
 *           "maxWidth": "400"
 *         }
 *       }
 *     }
 *   </script>
 *
 * - OR -
 *
 *   define(['restrictHeight'], function(createRestrictHeight) {
 *      return function() {
 *          const config = {
 *              "toggleBtn": ".btn__show-all",
 *              "initiallyRestricted": "false",
 *              "minWidth": "401",
 *              "programatic" : "true"
 *          };
 *          const element = $('.product-specs-list')
 *          const RestrictHeight = createRestrictHeight(config, element)
 *      }
 *   })
 *
 * EVENTS
 *
 *   There are 4 events that are thrown. They are:
 *
 *   - before_aligent_restrict-height_inline-style
 *       Thrown before the inline maxHeight style is added/removed from the element
 *
 *
 *   - after_aligent_restrict-height_inline-style
 *       Thrown after the inline maxHeight style is added/removed from the element
 *
 *
 *   - before_aligent_restrict-height_toggle
 *       Thrown before the toggle class name is added/remove from the element
 *
 *
 *   - after_aligent_restrict-height_toggle
 *       Thrown after the toggle class name is added/remove from the element
 *
 */
define(['jquery'], function($) {
    'use strict';

    class RestrictHeight {

        /**
         * Constructor
         *
         * @param {Object}  config  Configuration options passed in through JSON
         * @param {Element} element The element that this class was bound to
         *
         * @returns {null}
         */
        constructor(config, element) {
            const defaultConfig = {};
            // Merge default config options and provided config
            this.config         = {...defaultConfig, ...config};
            this.$element       = $(element);
            this.$toggleBtn     = $(this.config.toggleBtn);

            // Grab min and max widths, if not defined, set to extreme values that shouldn't ever be met
            this.maxWidth = (typeof this.config.maxWidth !== 'undefined') ? this.config.maxWidth : 9999999;
            this.minWidth = (typeof this.config.minWidth !== 'undefined') ? this.config.minWidth : 0;

            // Grab the initial max height inline setting. This will likely be blank, but is used when enabling height restriction again
            this.initialMaxHeight = element.style.maxHeight;

            // If the class is set to run in "programatic" mode, this means it's being controlled by another
            // JS class, and therefore shouldn't attach it's own event handlers
            if (this.config.programatically !== 'true') {
                this.attachListeners();
            }
        }

        /**
         * Attach event listeners to elements
         *
         * @returns {null}
         */
        attachListeners() {
            this.$toggleBtn.on('click', () => {
                this.toggleState();
            });
        }

        /**
         * Calculate the max-height for an element that is about to expand
         *
         * @returns {int}
         */
        calculateMaxHeight() {
            const maxHeight = this.$element.find('> *').last()[0].getBoundingClientRect().bottom - this.$element.find('> *').first()[0].getBoundingClientRect().top;

            // If the current inline maxHeight is the same as the initial, then the element is being changed to its
            // non height restricted state, so set the max-height that will allow for a smooth CSS transition to
            return (this.initialMaxHeight === this.$element[0].style.maxHeight)
                ? (maxHeight + 15) + 'px'
                : this.initialMaxHeight;
        }

        /**
         * Check if the width requirements provided (or not provided) are met by the current viewport
         *
         * @returns {boolean}
         */
        meetsWidthRequirements() {
            return window.innerWidth <= this.maxWidth && window.innerWidth >= this.minWidth;
        }

        /**
         * Toggle the state of the height restriction
         *
         * @returns {null}
         */
        toggleState() {
            this._throwEvent('before_aligent_restrict-height_inline-style');

            if (
                (typeof this.config.initiallyRestricted === 'undefined' || this.config.initiallyRestricted === 'true')
                && this.meetsWidthRequirements()
            ) {
                this.$element[0].style.maxHeight = this.calculateMaxHeight();
            }

            this._throwEvent('after_aligent_restrict-height_inline-style');

            // Wrap in a timeout to ensure the above style is rendered to the page before this the toggle class is added/removed
            window.setTimeout(() => {
                this._throwEvent('before_aligent_restrict-height_toggle');

                this.$element.toggleClass('toggle-height-restriction');
                this.$toggleBtn.toggleClass('toggle-height-restriction');

                if (this.config.initiallyRestricted === 'false' && this.meetsWidthRequirements()) {
                    // Due to it becoming too involved to implement a way to go from normally opened to height restricted,
                    // while also retaining the ability to use CSS transitions, I've had to use jQuery for the slide animation
                    this.$element.slideToggle();
                }

                this._throwEvent('after_aligent_restrict-height_toggle');
            }, 10);
        }

        /**
         *
         * @param {string} event Name of event to throw
         * @param {Object} extraParams [Optional] Extra parameters to send with the event
         *
         * @private
         * @returns {null}
         */
        _throwEvent(event, extraParams = {}) {
            this.$element.trigger(event, extraParams);
        }
    }

    return function(config, element) {
        var restrictHeight = new RestrictHeight(config, element);
        return restrictHeight;
    };
});
