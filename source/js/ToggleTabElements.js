/**
 * Show Hide Element
 *
 * Class to handle showing/hiding an element
 *
 * Will simply toggle a class (show-hide--toggle) on an element. To actually show/hide the element, CSS will need to be written.
 * This means that any desired animations can be created
 * You're free to interpret the class as you wish. It can either be used to hide the element (therefore visible by default)
 * or it could be used to show the element (therefore hidden by default), or using media queries, you could have the class
 * act differently depending on the users device width (i.e. mobile or not)
 *
 * PARAMETERS
 *
 *   elementToToggle  A DOM selector for the element that will have the show/hide class toggled on it
 *   elementDirection [Optional] String representing where the element to toggle is in relation to the element that this class was instantiated upon. Options are:
 *                    PLEASE NOTE: Untested when working with the elementTrigger parameter
 *                    self    Select the element that this class was instantiated upon
 *                    parent  Select a parent of the element (will recursively work up the parent tree until match found)
 *                    child   Select a child of the element. This is also recursive, doesn't have to be direct child
 *                    sibling Select a sibling. This will be an element that shares the same parent
 *                    [default] If no elementDirection is supplied, then the element will be queried against the document
 *   elementTrigger   [Optional] DOM selector for an element that will be used to trigger the ShowHideToggle class. This will be
 *                    required when dealing with DOM elements that are loaded in dynamically, allowing event listeners to be deferred
 *
 * USAGE
 *
 *   <button class="some-button" data-mage-init='"showHideElement": {"elementToToggle": ".element-to-show-hide"}'></button>
 *   ...
 *   <div class="element-to-show-hide">Content somewhere else in the DOM</div>
 *
 *   // Add the following somewhere in your CSS. Specificity is up to you.
 *   .show-hide--toggle {
 *     display: none; // You can choose how you want the element to be hidden. This essentially means visible by default
 *   }
 *
 *   OR
 *
 *   <script type="text/x-magento-init">
 *     {
 *         ".some-button": { //The element to bind the class to
 *             "showHideElement": {
 *                 "elementToToggle": ".element-to-show-hide",
 *                 "elementDirection": "[blank]|self|parent|child|sibling"
 *             }
 *         }
 *     }
 *   </script>
 *
 *   OR
 *
 *   define(['showHideElement'], function(createShowHideElement) {
 *      return function() {
 *          var config = {
 *              "elementToToggle": ".element-to-show-hide",
 *              "elementDirection": "[blank]|self|parent|child|sibling"
 *          };
 *          var element = $('.some-button')
 *          var ShowHideElement = createShowHideElement(config, element)
 *      }
 *   })
 *
 * EVENTS
 *
 *   There are 6 events that are thrown, with 4 thrown at any one time. The six events are:
 *
 *   - before_aligent_showhide_toggle
 *       Thrown before the toggle is executed
 *
 *   - after_aligent_showhide_toggle
 *       Thrown after the toggle is executed
 *
 *   - before_aligent_showhide_hide
 *       Thrown before a hide toggle is executed
 *
 *   - after_aligent_showhide_hide
 *       Thrown after a hide toggle is executed
 *
 *   - before_aligent_showhide_show
 *       Thrown before a show toggle is executed
 *
 *   - after_aligent_showhide_show
 *       Thrown after a show toggle is executed
 *
 * NOTE
 *
 *   This class will only select the first elementToToggle that matches the DOM selector, so you will need to ensure it
 *   is specific enough to grab the exact element that you want
 */
define(['jquery'], function($) {
    'use strict';

    class ShowHideElement {

        /**
         * Constructor
         *
         * @param {Object}  config  Configuration options passed in through JSON
         * @param {Element} element The element that this class was bound to
         *
         * @returns {null}
         */
        constructor(config, element) {

            const defaultConfig = {
                elementDirection: '',
                elementToToggle: ''
            };
            // Merge default config options and provided config
            this.config  = {...defaultConfig, ...config};
            this.$element = $(element);

            this.$elementToToggle = this.selectToggleElement();
            this.attachListener();
        }

        /**
         * Select the element that will have the show/hide class toggled on it
         *
         * @return {jQuery}
         */
        selectToggleElement() {

            // If an elementDirection isn't set, default to searching from the global document
            if (typeof this.config.elementDirection === 'undefined') {
                return $(this.config.elementToToggle);
            }

            // If toggling class-name on the same element that instantiated this class
            if (this.config.elementDirection === 'self') {
                return this.$element;
            }

            // If the element to toggle is a child
            if (this.config.elementDirection === 'child') {
                return this.$element.find(this.config.elementToToggle)
            }

            // If the element to toggle is a parent
            if (this.config.elementDirection === 'parent') {
                return this.$element.closest(this.config.elementToToggle);
            }

            // If the element to toggle is a sibling
            if (this.config.elementDirection === 'sibling') {
                return this.$element.parent().find(this.config.elementToToggle);
            }

            // If none of the above matches, then default to selecting the element using document
            return $(this.config.elementToToggle);
        }

        /**
         * Attach event listener to element
         *
         * @returns {null}
         */
        attachListener() {
            if (typeof this.config.elementTrigger !== 'undefined') {
                this.$element.on('click', this.config.elementTrigger, e => {
                    e.preventDefault();
                    this.toggleElement();
                });
            } else {
                this.$element.on('click', e => {
                    e.preventDefault();
                    this.toggleElement();
                });
            }

        }

        /**
         * Toggle the class on the element which indicates if it is visible or not
         *
         * @returns {null}
         */
        toggleElement() {
            this.throwEvent('before_aligent_showhide_toggle');

            if (this.$elementToToggle.hasClass('show-hide--toggle')) {
                this.throwEvent('before_aligent_showhide_hide');
                this.$elementToToggle.removeClass('show-hide--toggle');
                this.throwEvent('after_aligent_showhide_hide');
            } else {
                this.throwEvent('before_aligent_showhide_show');
                this.$elementToToggle.addClass('show-hide--toggle');
                this.throwEvent('after_aligent_showhide_show');
            }

            this.throwEvent('after_aligent_showhide_toggle');
        }

        /**
         * Throw an event that can be used to hook into the show/hide action
         *
         * @param {string} eventName The name of the event to throw
         *
         * @returns {null}
         */
        throwEvent(eventName) {
            this.$elementToToggle.trigger(eventName);
        }
    }

    return function(config, element) {
        var showHideElement = new ShowHideElement(config, element);
        return showHideElement;
    };
});
